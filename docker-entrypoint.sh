#!/bin/sh
set -e

# Get all station names of the json configuration file
stations=$(jq -r 'keys[]' config.json | paste -sd '|')
# Remove series of the stations which aren't in the file
influx \
    -host $INFLUXDB_HOST \
    -port $INFLUXDB_PORT \
    -username $INFLUXDB_USER \
    -password $INFLUXDB_PASS \
    -database $INFLUXDB_DB \
    -execute "drop series where code "'!'"~ /$stations/"

exec /entrypoint.sh "$@"
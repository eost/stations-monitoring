#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "libstrucs.h"


unsigned int calculate_CRC(char* command, long unsigned int command_size){
	crc_table_type *crc_table;
	crc_table = malloc(sizeof(crc_table_type));
	gcrcinit(crc_table);

	byte *buffer;
	buffer = malloc(command_size);
	memcpy(buffer, command, command_size);

	return gcrccalc(crc_table, buffer, command_size);
}


int main(int argc,char *argv[]){
	if(argc != 2){
    	printf("Usage: %s string_to_hash\n", argv[0]);
		printf("Example: %s 38020008000000000002000000000F2B\n", argv[0]);
        return 1;
    }

	char* hex_command = argv[1];
	long unsigned int command_size = strlen(hex_command) / 2;

	unsigned char command[command_size];
	size_t i;

	// Convert hex to bytes
	for(i = 0; i < command_size; i++){
        sscanf(hex_command + i*2, "%2hhx", &command[i]);
    }

	printf("%x\n", calculate_CRC(command, command_size));

    return 0;
}

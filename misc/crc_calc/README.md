CRC_CALC
========

`crc_calc` calculates a CRC hash used in Q330 requests.

Compilation
-----------

Juste use `make`…

Use
---

Easy! Just give the string in arguments.

```bash
$ ./crc_calc 38020008000000000002000000000F2B
54bb5ff0
```

FROM telegraf:1.8-alpine

ENV LANG C.UTF-8
ENV WORK_DIR /opt/monitoring
ENV CONFIG_FILE $WORK_DIR/config.json

COPY . $WORK_DIR

WORKDIR $WORK_DIR

RUN set -ex \
    && buildDeps=' \
        build-base \
        python3-dev \
        libxslt-dev \
    ' \
    && apk --no-cache add \
        python3 \
        libxslt \
        # Dependencies to remove former stations
        coreutils \
        jq \
        $buildDeps \
    # Install influxdb from edge
    && apk --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/latest-stable/community add \
        influxdb \
    && python3 -m ensurepip \
    && pip3 install --upgrade pip \
    && pip3 install --no-cache-dir --upgrade -r requirements.txt \
    && pip3 install . \
    && pip3 uninstall -y pip \
    && apk del $buildDeps

ENTRYPOINT ["/opt/monitoring/docker-entrypoint.sh"]
CMD ["telegraf"]
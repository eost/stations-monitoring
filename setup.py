#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(name='influxdb_importer',
      version='1.0',
      packages=find_packages(),
      entry_points={
          'console_scripts': [
              'influxdb_importer = monitoring.influxdb.influxdb_importer:main',
              'check_device = monitoring.influxdb.influxdb_importer:check_device',
          ],
      },
      )

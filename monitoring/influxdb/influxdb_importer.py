#!/usr/bin/env python3
"""
Import monitoring data into an InfluxDB database.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""
from abc import ABC, abstractmethod
import argparse
import json
import logging
import os

import aiohttp
import asyncio
from influxdb.line_protocol import make_lines

from monitoring.core.centaur import Centaur
from monitoring.core.guralp import Guralp
from monitoring.core.q330 import Q330
from monitoring.core.taurus import Taurus


logger = logging.getLogger(__name__)


class ConfigFileException(Exception):
    """Raise when config file can't be read"""


class Importer(ABC):
    """
    Retrieve data from a seismologic device and print the monitoring
    data using the InfluxDB line protocol.
    """
    def __init__(self, network, station, location, host, port=None,
                 projects=None, supply=None):
        self.device = None
        self.network = network
        self.station = station
        self.location = location
        self.projects = projects or []
        self.supply = supply
        self.host = host
        self.port = port or 80

        self.points = []

        self.init_device()

    @abstractmethod
    def init_device(self):
        """
        Set the device attribute.
        """

    def add_storage_point(self, measurement, storage, tags=None):
        """
        Add a `point` for the InfluxDB database which contains
        storage information (storage size, free space, etc).
        """
        fields = {}
        if tags is None:
            tags = {}

        if storage.size is not None:
            fields['size'] = storage.size

        if storage.free is not None:
            fields['free'] = storage.free

        if storage.used is not None:
            fields['used'] = storage.used

        if storage.percentage_free is not None:
            fields['percentage_free'] = storage.percentage_free

        if storage.percentage_used is not None:
            fields['percentage_used'] = storage.percentage_used

        if fields:
            self.add_point(measurement, fields, tags)

    def add_voltage_point(self, measurement, value, tags=None):
        """
        Add a `point` for the InfluxDB database which contains voltage
        information.
        """
        if tags is None:
            tags = {}

        if self.supply:
            tags['supply'] = self.supply

        try:
            self.add_point(measurement, float(value), tags)
        except TypeError:
            pass

    def add_point(self, measurement, value, tags=None):
        """
        Add a `point` for the InfluxDB database. It contains a
        measurement, tags and fields.
        """
        if value is None:
            return

        if tags is None:
            tags = {}

        if isinstance(value, dict):
            fields = value
        else:
            fields = {'value': value}

        point = {}
        point['measurement'] = measurement
        point['tags'] = tags
        point['tags']['network'] = self.network
        point['tags']['station'] = self.station
        point['tags']['location'] = self.location
        point['tags']['code'] = \
            '%s.%s.%s' % (self.network, self.station, self.location)
        point['tags']['projects'] = ','.join(self.projects)
        point['tags']['digitizer'] = self.device.name
        point['fields'] = fields

        logger.debug('Add point %s' % point)

        self.points.append(point)

    @abstractmethod
    def add_points(self):
        """
        Add all InfluxDB points of a device.
        """

    async def load_data(self, session):
        """
        Load data from a device.
        """
        logger.info('Load data for %s.%s' % (self.network, self.station))
        self.points = []
        await self.device.load_data(session, self.host, self.port)
        self.add_points()

    async def check_data_loading(self, session):
        """
        Check that data can be loaded from the device by printing the
        data values.
        """
        await self.load_data(session)
        print(self)

    async def import_data(self, session):
        """
        Retrieve data from a device and print it following the InfluxDB
        line protocol.
        """
        await self.load_data(session)

        logger.info('Write points for %s.%s' % (self.network, self.station))
        print(make_lines({'points': self.points}))

    def __str__(self):
        r = ''

        sorted_points = sorted(self.points, key=lambda k: k['measurement'])
        for point in sorted_points:
            default_tags = ['network', 'station', 'location', 'code',
                            'projects', 'digitizer']
            tags = {i: point['tags'][i] for i in point['tags']
                    if i not in default_tags}
            if tags:
                r += '%s %s:' % (point['measurement'], tags)
            else:
                r += '%s:' % point['measurement']

            if len(point['fields']) == 1 and 'value' in point['fields']:
                r += ' %s\n' % point['fields']['value']
            else:
                r += '\n'
                for v, k in point['fields'].items():
                    r += '    %s: %s\n' % (v, k)
        return r[:-1]


class CentaurImporter(Importer):
    """
    Import data from a Centaur device.
    """
    def init_device(self):
        self.device = Centaur()

    def add_points(self):
        data = self.device.data

        self.add_point('ping', data.ping)
        self.add_point('packet_number', data.packet_number)
        self.add_storage_point('memory', data.memory)
        self.add_storage_point('storage', data.store,
                               {'storage_type': 'store'})
        self.add_point('mass', data.mass_1, {'mass': 'mass_1'})
        self.add_point('mass', data.mass_2, {'mass': 'mass_2'})
        self.add_point('mass', data.mass_3, {'mass': 'mass_3'})
        self.add_point('external_soh', data.external_soh_voltage_1,
                       {'external_soh': 'voltage_1'})
        self.add_point('external_soh', data.external_soh_voltage_2,
                       {'external_soh': 'voltage_2'})
        self.add_point('external_soh', data.external_soh_voltage_3,
                       {'external_soh': 'voltage_3'})
        self.add_point('satellites', data.satellites)
        self.add_storage_point('storage', data.internal_storage,
                               {'storage_type': 'internal'})
        self.add_storage_point('storage', data.os_storage,
                               {'storage_type': 'OS'})
        self.add_storage_point('storage', data.removable_SD_storage,
                               {'storage_type': 'removable_SD'})
        self.add_voltage_point('voltage', data.system_voltage,
                               {'device': 'system'})
        self.add_point('current', data.system_current, {'device': 'system'})
        self.add_point('temperature', data.system_temperature,
                       {'device': 'system'})
        self.add_point('dac_count', data.dac_count)
        self.add_point('time_error', data.time_error)
        self.add_point('time_quality', data.time_quality)
        self.add_point('time_uncertainty', data.time_uncertainty)


class GuralpImporter(Importer):
    """
    Import data from a Güralp device.
    """
    def __init__(self, network, station, location, host, port=None,
                 projects=None, supply=None):
        port = port or 8080
        super().__init__(network, station, location, host, port, projects,
                         supply)

    def init_device(self):
        self.device = Guralp()

    def add_points(self):
        self.add_point('ping', self.device.data.ping)

        for i, data in enumerate(self.device.data.instruments):
            self.add_point('time_uncertainty', data.time_uncertainty,
                           {'instrument': 'instrument_%s' % i})
            self.add_point('mass', data.mass_z,
                           {'instrument': 'instrument_%s' % i,
                            'mass': 'mass_z'})
            self.add_point('mass', data.mass_n,
                           {'instrument': 'instrument_%s' % i,
                            'mass': 'mass_n'})
            self.add_point('mass', data.mass_e,
                           {'instrument': 'instrument_%s' % i,
                            'mass': 'mass_e'})

        data = self.device.data.ntp
        self.add_point('time_error', data.time_error)

        data = self.device.data.storage
        self.add_storage_point('storage', data.storage)

        data = self.device.data.system
        self.add_point('uptime', data.uptime, {'device': 'system'})
        self.add_point('load_average', data.load_average, {'device': 'system'})
        self.add_storage_point('storage', data.root_storage,
                               {'storage_type': 'root'})
        self.add_point('temperature', data.temperature, {'device': 'system'})
        self.add_voltage_point('voltage', data.voltage, {'device': 'system'})
        self.add_point('current', data.current, {'device': 'system'})
        self.add_point('power', data.power, {'device': 'system'})


class Q330Importer(Importer):
    """
    Import data from a Q330 device.
    """
    def __init__(self, network, station, location, host, port=None,
                 projects=None, supply=None):
        port = port or 5331
        super().__init__(network, station, location, host, port, projects,
                         supply)

    def init_device(self):
        self.device = Q330()

    def add_float_point(self, measurement, value, tags=None):
        try:
            self.add_point(measurement, float(value), tags)
        except TypeError:
            pass

    def add_points(self):
        data = self.device.data

        self.add_point('ping', data.ping)
        self.add_float_point('time_quality', data.time_quality)
        self.add_point('uptime', data.uptime, {'device': 'system'})
        self.add_point('satellites', data.satellites)
        self.add_float_point('mass', data.mass_1, {'mass': 'mass_1'})
        self.add_float_point('mass', data.mass_2, {'mass': 'mass_2'})
        self.add_float_point('mass', data.mass_3, {'mass': 'mass_3'})
        self.add_float_point('mass', data.mass_4, {'mass': 'mass_4'})
        self.add_float_point('mass', data.mass_5, {'mass': 'mass_5'})
        self.add_float_point('mass', data.mass_6, {'mass': 'mass_6'})
        self.add_voltage_point('voltage', data.system_voltage,
                               {'device': 'system'})
        self.add_float_point('temperature', data.system_temperature,
                             {'device': 'system'})
        self.add_float_point('current', data.system_current,
                             {'device': 'system'})
        self.add_float_point('current', data.gps_antenna_current,
                             {'device': 'gps_antenna'})
        self.add_float_point('temperature', data.seismometer_1_temperature,
                             {'device': 'seismometer_1'})
        self.add_float_point('temperature', data.seismometer_2_temperature,
                             {'device': 'seismometer_2'})
        self.add_point('initial_vco', data.initial_vco)
        self.add_point('time_error', data.time_error)
        for i, dp_memory in enumerate(data.dp_memory):
            self.add_storage_point('storage', dp_memory,
                                   tags={'storage_type': 'dp_memory_%s' % i})


class TaurusImporter(Importer):
    """
    Import data from a Taurus device.
    """
    def __init__(self, network, station, location, host, port=None,
                 projects=None, supply=None):
        port = port or 8080
        super().__init__(network, station, location, host, port, projects,
                         supply)

    def init_device(self):
        self.device = Taurus()

    def add_points(self):
        data = self.device.data

        self.add_point('ping', data.ping)
        self.add_point('store_time_left', data.store_time_left)
        self.add_storage_point('storage', data.store)
        self.add_point('power', data.system_power, {'device': 'system'})
        self.add_point('temperature', data.system_temperature,
                       {'device': 'system'})
        self.add_voltage_point('voltage', data.system_voltage,
                               {'device': 'system'})
        self.add_point('current', data.sensor_current, {'device': 'sensor'})
        self.add_point('current', data.digitizer_current,
                       {'device': 'digitizer'})
        self.add_point('current', data.system_current, {'device': 'system'})
        self.add_point('time_uncertainty', data.time_uncertainty)
        self.add_point('time_error', data.time_error)
        self.add_point('dac_count', data.dac_count)
        self.add_point('satellites', data.satellites)
        self.add_point('pdop', data.pdop)
        self.add_point('tdop', data.tdop)
        self.add_point('mass', data.mass_1, {'mass': 'mass_1'})
        self.add_point('mass', data.mass_2, {'mass': 'mass_2'})
        self.add_point('mass', data.mass_3, {'mass': 'mass_3'})
        self.add_point('power', data.sensor_power, {'device': 'sensor'})


class InfluxDBManager:
    """
    Retrieve data from several devices and print it following the
    InfluxDB line protocol.
    """
    IMPORTERS = {
        'centaur': CentaurImporter,
        'guralp': GuralpImporter,
        'q330': Q330Importer,
        'taurus': TaurusImporter,
    }

    def __init__(self):
        self.importers = []

    def add_device(self, network, station, location, device, host, port=None,
                   projects=None, supply=None):
        """
        Add a device to the list of importers.
        """
        try:
            importer_cls = InfluxDBManager.IMPORTERS[device]
        except KeyError:
            logger.error('Unknown device %s' % device)
        else:
            importer = importer_cls(network, station, location, host, port,
                                    projects=projects, supply=supply)

            self.importers.append(importer)

    def load_json(self, filename):
        """
        Read the configuration of the devices from a json file.
        """
        if filename is None:
            try:
                filename = os.environ['CONFIG_FILE']
            except KeyError:
                message = ("No config file, specified, the environment "
                           "variable `CONFIG_FILE` does not exist.")
                raise ConfigFileException(message)

        try:
            with open(filename) as config_file:
                devices = json.load(config_file)

                for name, data in devices.items():
                    network, station, location = name.split('.')
                    device = data['device']
                    host = data['host']
                    try:
                        port = data['port']
                    except KeyError:
                        port = None
                    try:
                        projects = data['projects']
                    except KeyError:
                        projects = None
                    try:
                        supply = data['supply']
                    except KeyError:
                        supply = None

                    self.add_device(network, station, location, device, host,
                                    port, projects, supply)
        except (AttributeError, KeyError, ValueError):
            raise ConfigFileException("Can't parse json file")
        except OSError:
            raise ConfigFileException("Can't open config file")

    async def check_data_loading(self, loop):
        """
        Check that data can be retrieved from the devices.

        The `loop` parameter is the asyncio event loop.
        """
        async with aiohttp.ClientSession(loop=loop) as session:
            coroutines = []
            for importer in self.importers:
                coroutines.append(importer.check_data_loading(session))

            try:
                await asyncio.wait(coroutines)
            except ValueError:
                logger.warning('No devices config found')

    async def import_data(self, loop):
        """
        Retrieve all devices data and print the InfluxDB lines.

        The `loop` parameter is the asyncio event loop.
        """
        logger.info('Start import')

        async with aiohttp.ClientSession(loop=loop) as session:
            coroutines = []
            for importer in self.importers:
                coroutines.append(importer.import_data(session))

            try:
                await asyncio.wait(coroutines)
            except ValueError:
                logger.warning('No devices config found')

        logger.info('Import done')


def check_device():
    """
    Check data retrieval for a device.
    """
    parser = argparse.ArgumentParser(
        description=('Retrieve monitoring data from a specific device'),
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('device',
                        help='Type of the device',
                        choices=['centaur', 'q330', 'guralp', 'taurus'])
    parser.add_argument('host',
                        help='Hostname of the device')
    parser.add_argument('-p', '--port',
                        help='Port of the device',
                        type=int)
    parser.add_argument('-v', '--verbose',
                        help=('Can be supplied multiple time to increase '
                              'verbosity (default: ERROR).'),
                        action='count', default=0)

    args = parser.parse_args()
    device = args.device
    host = args.host
    port = args.port
    verbose = args.verbose

    # Set logging level
    levels = [logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]
    log_level = levels[min(len(levels) - 1, verbose)]
    log_format = '\033[2K%(asctime)s %(name)s: %(levelname)s: %(message)s'
    logging.basicConfig(level=log_level,
                        format=log_format,
                        datefmt="%Y-%m-%d %H:%M:%S")

    influxdb_manager = InfluxDBManager()
    influxdb_manager.add_device(None, None, None, device, host, port)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(influxdb_manager.check_data_loading(loop))
    loop.close()


def main():
    parser = argparse.ArgumentParser(
        description=('Retrieve monitoring data from different devices'),
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-f', '--filename',
                        help='Config file where devices are described')
    parser.add_argument('-v', '--verbose',
                        help=('Can be supplied multiple time to increase '
                              'verbosity (default: ERROR).'),
                        action='count', default=0)

    args = parser.parse_args()
    filename = args.filename
    verbose = args.verbose

    # Set logging level
    levels = [logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]
    log_level = levels[min(len(levels) - 1, verbose)]
    log_format = '\033[2K%(asctime)s %(name)s: %(levelname)s: %(message)s'
    logging.basicConfig(level=log_level,
                        format=log_format,
                        datefmt="%Y-%m-%d %H:%M:%S")

    influxdb_manager = InfluxDBManager()
    try:
        influxdb_manager.load_json(filename)
    except ConfigFileException as e:
        logger.error(e)
        exit(1)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(influxdb_manager.import_data(loop))
    loop.close()


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-
"""
Q330 monitoring core support.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""
import logging
import socket
from struct import unpack
import time

import asyncio
import async_timeout
from .device import Device, DeviceData, Storage


logger = logging.getLogger(__name__)


class Q330(Device):
    def init_name(self):
        self.name = 'q330'

    def init_data(self):
        self.data = Q330Data()


class Q330Data(DeviceData):
    """
    Store Q330 status data
    """
    SOCKET_BUFFER_SIZE = 1024

    def __init__(self):
        super().__init__()
        self.time_quality = None
        self.uptime = None
        self.satellites = None
        self.mass_1 = None
        self.mass_2 = None
        self.mass_3 = None
        self.mass_4 = None
        self.mass_5 = None
        self.mass_6 = None
        self.system_voltage = None
        self.system_temperature = None
        self.system_current = None
        self.gps_antenna_current = None
        self.seismometer_1_temperature = None
        self.seismometer_2_temperature = None
        self.initial_vco = None
        self.time_error = None
        self.dp_memory = [Storage() for _ in range(4)]

    def reset(self):
        super().reset()
        self.time_quality = None
        self.uptime = None
        self.satellites = None
        self.mass_1 = None
        self.mass_2 = None
        self.mass_3 = None
        self.mass_4 = None
        self.mass_5 = None
        self.mass_6 = None
        self.system_voltage = None
        self.system_temperature = None
        self.system_current = None
        self.gps_antenna_current = None
        self.seismometer_1_temperature = None
        self.seismometer_2_temperature = None
        self.initial_vco = None
        self.time_error = None
        for storage in self.dp_memory:
            storage.reset()

    async def send_socket(self, address, port, command):
        """
        Send a socket to the Q330 and return the response.
        """
        loop = asyncio.get_event_loop()

        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            # Need a non blocking socket for asyncio
            sock.setblocking(False)
            sock.sendto(command, (address, port))
            with async_timeout.timeout(Device.TIMEOUT):
                response = \
                    await loop.sock_recv(sock, Q330Data.SOCKET_BUFFER_SIZE)
        except socket.error as e:
            logger.warning(e)
            return None
        except asyncio.TimeoutError:
            logger.warning('TimeoutError for Q330 socket %s:%s' % (address,
                                                                   port))
            return None
        finally:
            sock.close()

        return response

    async def _send_status_request(self, address, port=5331):
        # Send a status request
        # 0-11     QDP header format
        #   0-3    CRC (hash calculated on the command with the crc_calc custom
        #          utility)
        #   4      Command (\x38 for PING)
        #   5      Version (2)
        #   6-7    Length of the data (8 bytes)
        #   8-9    Sequence number
        #   10-11  Acknowledge number
        # 12-19    Format of status request (PING)
        #   12-13  Type = 2 for a status request
        #   14-15  User message number
        #   16-19  Status request bitmap
        #          0F0B = 00001111 00101011
        status_command = (b'\x54\xBB\x5F\xF0\x38\x02\x00\x08\x00\x00\x00\x00'
                          b'\x00\x02\x00\x00\x00\x00\x0F\x2B')
        response = await self.send_socket(address, port, status_command)

        if response is None:
            return

        # Status response
        # 0-11     QDP header format
        #   0-3    CRC
        #   4      Command
        #   5      Version
        #   6-7    Length
        #   8-9    Sequence number
        #   10-11  Acknowledge number
        # 12-35    Format of status response
        #   0-1    Type = 3 for a status response
        #   2-3    User message number
        #   4-5    Drift tolerance
        #   6-7    User message count
        #   8-11   Time of last reboot
        #   12-19  Spare
        #   20-23  Status bitmap
        # 36-87    Global status
        #   0-1    Acquisition control
        #   2-3    Clock quality
        time_quality_factor = unpack('>H', response[38:40])[0]
        #   4-5    Minute since lost
        minutes_since_loss = unpack('>H', response[40:42])[0]
        if time_quality_factor & 1 == 0:
            self.time_quality = 0
        elif time_quality_factor & 5 == 5:
            self.time_quality = 100
        elif time_quality_factor & 3 == 3:
            self.time_quality = 90
        elif time_quality_factor & 9 == 9:
            self.time_quality = 80
        elif (time_quality_factor & 32 == 32) or \
                (time_quality_factor & 16 == 16):
            self.time_quality = 60 - minutes_since_loss / 10
            if self.time_quality < 10:
                self.time_quality = 10
        #   6-7    Analog voltage control value
        #   8-11   Seconds offset (32 bits)
        # From 2000/01/01 at 00:00:00, convert to epoch
        last_boot = unpack('>I', response[44:48])[0] + 946684800
        self.uptime = int(time.time()) - last_boot
        #   12-15  Usec offset (32 bits)
        #   16-19  Total time in seconds
        #   20-23  Power on time in seconds
        #   24-27  Time of last re-sync
        #   28-31  Total number of re-syncs
        #   32-33  GPS status
        #   34-35  Calibrator status
        #   36-37  Sensor bitmap
        #   38-39  Current VCO
        #   40-41  Data sequence number
        #   42-43  PLL running if set
        #   44-45  Status inputs
        #   46-47  Misc. inputs
        #   48-51  Current data sequence number
        # 88-171   GPS status
        #   0-1    GPS power on/off time
        #   2-3    GPS on if <> 0
        #   4-5    Number of satellites used
        self.satellites = unpack('>H', response[92:94])[0]
        #   6-7    Number of satellites in view
        #   8-17   GPS time string[9]
        #   18-29  GPS date string[11]
        #   30-35  GPS fix string[5]
        #   36-47  GPS height string[11]
        #   48-61  GPS latitude string[13]
        #   62-75  GPS longitude string[13]
        #   76-79  Time of last good 1PPS
        #   80-83  Total checksum errors
        # 172-203  Booms position, temperatures and voltages
        #   0-1    Channel 1 boom (in dV)
        self.mass_1 = unpack('>h', response[172:174])[0] / 10
        #   2-3    Channel 2 boom (in dV)
        self.mass_2 = unpack('>h', response[174:176])[0] / 10
        #   4-5    Channel 3 boom (in dV)
        self.mass_3 = unpack('>h', response[176:178])[0] / 10
        #   6-7    Channel 4 boom (in dV)
        self.mass_4 = unpack('>h', response[178:180])[0] / 10
        #   8-9    Channel 5 boom (in dV)
        self.mass_5 = unpack('>h', response[180:182])[0] / 10
        #   10-11  Channel 6 boom (in dV)
        self.mass_6 = unpack('>h', response[182:184])[0] / 10
        #   12-13  Analog positive supply (10mv increments)
        #   14-15  Analog negative supply if not zero
        #   16-17  Input power (150mv increments)
        self.system_voltage = unpack('>h', response[188:190])[0] * 0.15
        #   18-19  System temperature (celsius)
        system_temperature = unpack('>h', response[190:192])[0]
        if system_temperature != 666:
            self.system_temperature = system_temperature
        #   20-21  Main current (1ma increments)
        self.system_current = unpack('>h', response[192:194])[0] / 1e3
        #   22-23  GPS antenna current (1ma increments)
        self.gps_antenna_current = unpack('>h', response[194:196])[0] / 1e3
        #   24-25  Seismometer 1 temperature (celsius)
        seismometer_1_temperature = unpack('>h', response[196:198])[0]
        if seismometer_1_temperature != 666:
            self.seismometer_1_temperature = seismometer_1_temperature
        #   26-27  Seismometer 2 temperature (celsius)
        seismometer_2_temperature = unpack('>h', response[198:200])[0]
        if seismometer_2_temperature != 666:
            self.seismometer_2_temperature = seismometer_2_temperature
        #   28-31  Slave processor timeouts
        # 204-231  PLL status
        #   0-3    Initial VCO (float)
        self.initial_vco = unpack('>f', response[204:208])[0]
        #   4-7    Time error (float)
        self.time_error = unpack('>f', response[208:212])[0]
        #   8-11   RMS VCO (float)
        #   12-15  Best VCO (float)
        #   16-19  Spare
        #   20-23  Ticks since last update
        #   24-25  Km
        #   26-27  State
        # 232-263  Data port 1 status
        #   0-3    Total data packets sent
        #   4-7    Total packets re-sent
        #   8-11   Total fill packets sent
        #   12-15  Receive sequence errors
        #   16-19  Bytes of packet currently used
        self.dp_memory[0].used = unpack('>I', response[248:252])[0]
        #   20-23  Time of last data packet acked
        #   24-25  Physical interface number
        #   26-27  Data port number
        #   28-29  Retransmission timeout (100ms)
        #   30-31  Flags
        # 264-295  Data port 2 status
        #   0-3    Total data packets sent
        #   4-7    Total packets re-sent
        #   8-11   Total fill packets sent
        #   12-15  Receive sequence errors
        #   16-19  Bytes of packet currently used
        self.dp_memory[1].used = unpack('>I', response[280:284])[0]
        #   20-23  Time of last data packet acked
        #   24-25  Physical interface number
        #   26-27  Data port number
        #   28-29  Retransmission timeout (100ms)
        #   30-31  Flags
        # 296-327  Data port 3 status
        #   0-3    Total data packets sent
        #   4-7    Total packets re-sent
        #   8-11   Total fill packets sent
        #   12-15  Receive sequence errors
        #   16-19  Bytes of packet currently used
        self.dp_memory[2].used = unpack('>I', response[312:316])[0]
        #   20-23  Time of last data packet acked
        #   24-25  Physical interface number
        #   26-27  Data port number
        #   28-29  Retransmission timeout (100ms)
        #   30-31  Flags
        # 328-359  Data port 4 status
        #   0-3    Total data packets sent
        #   4-7    Total packets re-sent
        #   8-11   Total fill packets sent
        #   12-15  Receive sequence errors
        #   16-19  Bytes of packet currently used
        self.dp_memory[3].used = unpack('>I', response[344:348])[0]
        #   20-23  Time of last data packet acked
        #   24-25  Physical interface number
        #   26-27  Data port number
        #   28-29  Retransmission timeout (100ms)
        #   30-31  Flags

    async def _send_information_request(self, address, port=5331):
        # Send an information request
        # 0-11     QDP header format
        #   0-3    CRC (hash calculated on the command with the crc_calc custom
        #          utility)
        #   4      Command (\x38 for PING)
        #   5      Version (2)
        #   6-7    Length of the data (4 bytes)
        #   8-9    Sequence number
        #   10-11  Acknowledge number
        # 12-15    Format of status request (PING)
        #   12-13  Type = 4 for a information request
        #   14-15  Ignored
        information_command = (
            b'\x4D\xF9\xAA\xC0\x38\x02\x00\x04\x00\x00\x00\x00'
            b'\x00\x04\x00\x00')
        response = await self.send_socket(address, port, information_command)

        if response is None:
            return

        # Information response
        # 0-11     QDP header format
        #   0-3    CRC
        #   4      Command
        #   5      Version
        #   6-7    Length
        #   8-9    Sequence number
        #   10-11  Acknowledge number
        # 12-83    Format of information response
        #   0-1    Type = 5 for information response
        #   2-3    Ignored
        #   4-5    Version
        #   6-7    Flags
        #   8-11   KMI property tag
        #   12-15  Serial number (high 32 bits)
        #   16-19  Serial number (low 32 bits)
        #   20-23  Data port 1 packet memory size
        self.dp_memory[0].size = unpack('>I', response[32:36])[0]
        #   24-27  Data port 2 packet memory size
        self.dp_memory[1].size = unpack('>I', response[36:40])[0]
        #   28-31  Data port 3 packet memory size
        self.dp_memory[2].size = unpack('>I', response[40:44])[0]
        #   32-35  Data port 4 packet memory size
        self.dp_memory[3].size = unpack('>I', response[44:48])[0]
        #   36-39  Serial interface 1 memory trigger level
        #   40-43  Serial interface 2 memory trigger level
        #   44-47  Reserved
        #   48-51  Ethernet interface memory trigger level
        #   52-53  Serial interface 1 advanced flags
        #   54-55  Serial interface 2 advanced flags
        #   56-57  Reserved
        #   58-59  Ethernet interface advanced flags
        #   60-61  Serial interface 1 data port number
        #   62-63  Serial interface 2 data port number
        #   64-65  Reserved
        #   66-67  Ethernet interface data port number
        #   68-69  Calibration error bitmap
        #   70-71  System software version

    async def load_data(self, session, address, port=5331):
        """
        Send sockets to the Q330 and import data into this object.
        """
        coroutines = [
            self._send_status_request(address, port),
            self._send_information_request(address, port),
        ]
        await asyncio.wait(coroutines)

        for dp_memory in self.dp_memory:
            dp_memory.calculate_free()
            dp_memory.calculate_percentage_free()
            dp_memory.calculate_percentage_used()

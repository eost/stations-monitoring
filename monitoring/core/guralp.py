# -*- coding: utf-8 -*-
"""
Guralp monitoring core support.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""
from abc import ABC, abstractmethod
from datetime import datetime
import logging

import aiohttp
import asyncio
import async_timeout
import dateutil.parser
from lxml import etree

from . import device
from .device import Device, DeviceData


logger = logging.getLogger(__name__)


class Guralp(Device):
    def init_name(self):
        self.name = 'guralp'

    def init_data(self):
        self.data = GuralpData()


class GuralpData(DeviceData):
    """
    Store Guralp status data.
    """
    def __init__(self):
        super().__init__()
        self.port_A = Port('A')
        self.port_B = Port('B')
        self.scream_server = ScreamServer()
        self.gdi_link_tx = GdiLinkTransmitter()
        self.gcf_compressor = GCFCompressor()
        self.miniseed_compressor = MiniSEEDCompressor()
        self.instruments = [
            Instrument(1),
            Instrument(2),
            Instrument(3),
            Instrument(4),
        ]
        self.ntp = NTP()
        self.seedlink_network_server = SEEDLinkNetworkServer()
        self.storage = Storage()
        self.system = System()

    def reset(self):
        super().reset()
        self.port_A.reset()
        self.port_B.reset()
        self.scream_server.reset()
        self.gdi_link_tx.reset()
        self.gcf_compressor.reset()
        self.miniseed_compressor.reset()
        for instrument in self.instruments:
            instrument.reset()
        self.ntp.reset()
        self.seedlink_network_server.reset()
        self.storage.reset()
        self.system.reset()

    async def _get_root(self, session, url):
        """
        Retrieve the XML file of the Güralp device.
        """
        # Add POST data to get the XML
        post_data = {'download_xml': 'Download snapshot as XML'}
        try:
            with async_timeout.timeout(Device.TIMEOUT):
                async with session.post(url, data=post_data) as response:
                    xml_data = await response.text()
        except asyncio.TimeoutError:
            logger.warning('TimeoutError for %s' % url)
            return None
        except aiohttp.ClientError:
            logger.warning("Can't connect to %s" % url)
            return None

        try:
            return etree.fromstring(xml_data)
        except etree.XMLSyntaxError:
            logger.warning('Error while parsing XML %s' % url)
            return None

    async def load_data(self, session, address, port=8080):
        """
        Return a Guralp object containing the monitored data.
        """
        url = 'http://%s:%s/cgi-bin/xmlstatus.cgi' % (address, port)
        root = await self._get_root(session, url)
        if root is None:
            return

        self.port_A.parse(root)
        self.port_B.parse(root)
        self.scream_server.parse(root)
        self.gdi_link_tx.parse(root)
        self.gcf_compressor.parse(root)
        self.miniseed_compressor.parse(root)
        for instrument in self.instruments:
            instrument.parse(root)
            instrument.parse(root)
            instrument.parse(root)
            instrument.parse(root)
        self.ntp.parse(root)
        self.seedlink_network_server.parse(root)
        self.storage.parse(root)
        self.system.parse(root)


class Module(ABC):
    """
    Abstract class used to parse module block in XML file.
    """
    def __init__(self, module_path):
        self.module_path = module_path
        self.module = None
        self.namespaces = {
            'ns': 'http://www.guralp.com/platinum/xmlns/xmlstatus/1.1',
        }

    def load(function):
        """
        Decorator to load module before parsing. If the module can't be
        parsed, the parse method isn't call.
        """
        def wrapper(self, root):
            try:
                self.module = root.xpath(self.module_path,
                                         namespaces=self.namespaces)[0]
            except IndexError:
                pass
            else:
                function(self, root)
        return wrapper

    @abstractmethod
    @load
    def parse(self, root):
        """
        Parse the module and extract data.
        """
        pass

    def _parse_int(self, xpath, factor=1, divisor=1):
        element = self.module.xpath(xpath, namespaces=self.namespaces)
        try:
            return int(element[0]) * factor // divisor
        except (IndexError, TypeError, ValueError):
            return None

    def _parse_float(self, xpath, factor=1, divisor=1):
        element = self.module.xpath(xpath, namespaces=self.namespaces)
        try:
            return float(element[0]) * factor / divisor
        except (IndexError, TypeError, ValueError):
            return None

    def _parse_percent(self, xpath):
        element = self.module.xpath(xpath, namespaces=self.namespaces)
        try:
            return float(element[0][:-1])
        except (IndexError, TypeError, ValueError):
            return None

    def _parse_timestamp(self, xpath, factor=1, divisor=1):
        element = self.module.xpath(xpath, namespaces=self.namespaces)
        try:
            timestamp = dateutil.parser.parse(element[0])
        except IndexError:
            return None
        else:
            timestamp = timestamp.replace(tzinfo=None)
            delta = datetime.utcnow() - timestamp
            return delta.total_seconds() * factor / divisor


class Port(Module):
    def __init__(self, port_value):
        module_path = \
            "//ns:module[@path='gcf-in-brp.Port%s']" % port_value
        super().__init__(module_path)

        self.blocks_received = None
        self.bytes_received = None
        self.naks_sent = None
        self.packet_received_time = None

    def reset(self):
        self.blocks_received = None
        self.bytes_received = None
        self.naks_sent = None
        self.packet_received_time = None


    def parse(self, root):
        super().parse(root)

        self.blocks_received = \
            self._parse_int("./ns:reading[@path='last5_blocks_in']/text()")
        self.bytes_received = self._parse_int(
            "./ns:reading[@path='bytes_in']/text()")
        self.naks_sent = \
            self._parse_int("./ns:reading[@path='last5_naks']/text()")
        self.packet_received_time = self._parse_timestamp(
            "./ns:reading[@path='latest_timestamp']/text()")


class ScreamServer(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='gcf-out-scream.default']")

        self.blocks_sent = None
        self.clients = None

    def reset(self):
        self.blocks_sent = None
        self.clients = None

    def parse(self, root):
        super().parse(root)

        self.blocks_sent = \
            self._parse_int("./ns:reading[@path='last5_blocks']/text()")
        self.clients = \
            self._parse_int("./ns:reading[@path='num_clients']/text()")


class GdiLinkTransmitter(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='gdi-link-tx.default']")

        self.bytes_sent = None
        self.transmit_rate = None
        self.clients = None

    def reset(self):
        self.bytes_sent = None
        self.transmit_rate = None
        self.clients = None

    def parse(self, root):
        super().parse(root)

        self.bytes_sent = self._parse_int(
            "./ns:reading[@path='last5_bytes_sent']/text()")
        self.transmit_rate = \
            self._parse_float("./ns:reading[@path='tx_rate']/text()")
        self.clients = \
            self._parse_int("./ns:reading[@path='num_clients']/text()")


class GCFCompressor(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='gdi2gcf.default']")

        self.samples = None
        self.blocks_out = None

    def reset(self):
        self.samples = None
        self.blocks_out = None

    def parse(self, root):
        super().parse(root)

        self.samples = \
            self._parse_int("./ns:reading[@path='last5_samples_in']/text()")
        self.blocks_out = \
            self._parse_int("./ns:reading[@path='last5_blocks_out']/text()")


class MiniSEEDCompressor(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='gdi2miniseed.default']")

        self.samples = None
        self.text_samples = None
        self.records_out = None

    def reset(self):
        self.samples = None
        self.text_samples = None
        self.records_out = None

    def parse(self, root):
        super().parse(root)

        self.samples = \
            self._parse_int("./ns:reading[@path='last5_samples_in']/text()")
        self.text_samples = \
            self._parse_int("./ns:reading[@path='last5_text_in']/text()")
        self.records_out = \
            self._parse_int("./ns:reading[@path='last5_ms_rec_out']/text()")


class Instrument(Module):
    def __init__(self, instrument_nb):
        module_path = "//ns:module[contains(@path, 'instrument')][%s]" \
                      % instrument_nb
        super().__init__(module_path)

        self.time_uncertainty = None
        self.clock_last_locked_time = None
        self.mass_z = None
        self.mass_n = None
        self.mass_e = None
        self.tap_table_lookup = None

    def reset(self):
        self.time_uncertainty = None
        self.clock_last_locked_time = None
        self.mass_z = None
        self.mass_n = None
        self.mass_e = None
        self.tap_table_lookup = None

    def parse(self, root):
        super().parse(root)

        self.time_uncertainty = self._parse_float(
            "./ns:reading[@path='clock_diff']/text()")
        self.clock_last_locked_time = self._parse_timestamp(
            "./ns:reading[@path='clock_last_locked']/text()")
        self.mass_z = \
            self._parse_percent("./ns:reading[@path='mass_z']/text()")
        self.mass_n = \
            self._parse_percent("./ns:reading[@path='mass_n']/text()")
        self.mass_e = \
            self._parse_percent("./ns:reading[@path='mass_e']/text()")
        self.tap_table_lookup = \
            self._parse_int("./ns:reading[@path='tap_table_lookup']/text()")


class NTP(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='ntp']")

        self.time_error = None

    def reset(self):
        self.time_error = None

    def parse(self, root):
        super().parse(root)

        self.time_error = self._parse_float(
            "./ns:reading[@path='estimated_error']/text()")


class SEEDLinkNetworkServer(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='seedlink-out.0']")

        self.records = None
        self.clients = None

    def reset(self):
        self.records = None
        self.clients = None

    def parse(self, root):
        super().parse(root)

        self.records = \
            self._parse_int("./ns:reading[@path='last5_records']/text()")
        self.clients = \
            self._parse_int("./ns:reading[@path='num_clients']/text()")


class Storage(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='storage']")

        self.last_accessed_time = None
        self.storage = device.Storage()
        self.power_duty_cycle = None

    def reset(self):
        self.last_accessed_time = None
        self.storage.reset()
        self.power_duty_cycle = None

    def parse(self, root):
        super().parse(root)

        self.last_accessed_time = self._parse_timestamp(
            "./ns:reading[@path='last_accessed']/text()")
        self.storage.size = self._parse_int(
            "./ns:reading[@path='size']/text()")

        storage_free_path = "./ns:reading[@path='free_space']/text()"
        storage_free = self.module.xpath(storage_free_path,
                                         namespaces=self.namespaces)
        try:
            self.storage.free = int(storage_free[0])
        except (IndexError, TypeError, ValueError):
            pass

        power_duty_cycle_path = "./ns:reading[@path='power_duty_cycle']/text()"
        self.power_duty_cycle = self._parse_percent(power_duty_cycle_path)

        self.power_on_time = \
            self._parse_int("./ns:reading[@path='power_on_time']/text()")

        self.storage.calculate_used()
        self.storage.calculate_percentage_free()
        self.storage.calculate_percentage_used()


class System(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='system']")

        self.uptime = None
        self.load_average = None
        self.root_storage = device.Storage()
        self.temperature = None
        self.voltage = None
        self.current = None
        self.power = None

    def reset(self):
        self.uptime = None
        self.load_average = None
        self.root_storage.reset()
        self.temperature = None
        self.voltage = None
        self.current = None
        self.power = None

    def parse(self, root):
        super().parse(root)

        self.uptime = self._parse_int("./ns:reading[@path='uptime']/text()")
        self.load_average = \
            self._parse_float("./ns:reading[@path='load_average']/text()")
        self.root_storage.free = self._parse_int(
            "./ns:reading[@path='root_free_space']/text()")
        self.root_storage.percentage_free = self._parse_percent(
            "./ns:reading[@path='root_percent_free_space']/text()")
        self.temperature = \
            self._parse_float("./ns:reading[@path='temperature']/text()")
        self.voltage = \
            self._parse_float("./ns:reading[@path='systemvoltage']/text()")
        self.current = self._parse_float(
            "./ns:reading[@path='systemcurrent']/text()")
        self.power = \
            self._parse_float("./ns:reading[@path='systempower']/text()")

        self.root_storage.calculate_size_from_percentage_free()
        self.root_storage.calculate_used()
        self.root_storage.calculate_percentage_used()

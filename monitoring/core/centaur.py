# -*- coding: utf-8 -*-
"""
Centaur monitoring core support.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""
import json
import logging

import aiohttp
import asyncio
import async_timeout

from .device import Device, DeviceData, Storage


logger = logging.getLogger(__name__)


class Centaur(Device):
    def init_name(self):
        self.name = 'centaur'

    def init_data(self):
        self.data = CentaurData()


class CentaurData(DeviceData):
    """
    Store Centaur status data.
    """
    SECONDS = {
        'http://nmx.ca/05/units/seconds': 1,
        'http://nmx.ca/05/units/milliseconds': 1e-3,
        'http://nmx.ca/05/units/microseconds': 1e-6,
        'http://nmx.ca/05/units/nanoseconds': 1e-9,
    }

    VOLTS = {
        'http://nmx.ca/05/units/volts': 1,
        'http://nmx.ca/05/units/millivolts': 1e-3,
        'http://nmx.ca/05/units/microvolts': 1e-6,
    }

    UNITS = {**SECONDS, **VOLTS}

    def __init__(self):
        super().__init__()
        self.packet_number = None
        self.memory = Storage()
        self.store = Storage()
        self.mass_1 = None
        self.mass_2 = None
        self.mass_3 = None
        self.external_soh_voltage_1 = None
        self.external_soh_voltage_2 = None
        self.external_soh_voltage_3 = None
        self.satellites = None
        self.internal_storage = Storage()
        self.os_storage = Storage()
        self.removable_SD_storage = Storage()
        self.system_voltage = None
        self.system_current = None
        self.system_temperature = None
        self.dac_count = None
        self.time_error = None
        self.time_quality = None
        self.time_uncertainty = None

    def reset(self):
        super().reset()
        self.packet_number = None
        self.memory.reset()
        self.store.reset()
        self.mass_1 = None
        self.mass_2 = None
        self.mass_3 = None
        self.external_soh_voltage_1 = None
        self.external_soh_voltage_2 = None
        self.external_soh_voltage_3 = None
        self.satellites = None
        self.internal_storage.reset()
        self.os_storage.reset()
        self.removable_SD_storage.reset()
        self.system_voltage = None
        self.system_current = None
        self.system_temperature = None
        self.dac_count = None
        self.time_error = None
        self.time_quality = None
        self.time_uncertainty = None

    def _parse_int(self, data, key, factor=1, divisor=1):
        value = None
        try:
            value = int(data[key]['value']) * factor // divisor
        except (KeyError, TypeError, ValueError):
            return None

        try:
            unit = data[key]['units']
            value = value * CentaurData.UNITS[unit]
        except KeyError:
            pass

        return value

    def _parse_float(self, data, key, factor=1, divisor=1):
        value = None
        try:
            value = float(data[key]['value']) * factor / divisor
        except (KeyError, TypeError, ValueError):
            return None

        try:
            unit = data[key]['units']
            value = value * CentaurData.UNITS[unit]
        except KeyError:
            pass

        return value

    async def load_data(self, session, address, port=80):
        """
        Retrieve the json file from the Centaur device and import data
        into the Centaur object.
        """
        url = 'http://%s:%s/api/v1/instruments/soh' % (address, port)

        try:
            with async_timeout.timeout(Device.TIMEOUT):
                async with session.get(url) as response:
                    json_data = await response.text()
        except asyncio.TimeoutError:
            logger.warning('TimeoutError for %s' % url)
            return
        except aiohttp.ClientError:
            logger.warning("Can't connect to %s" % url)
            return

        try:
            data = json.loads(json_data)
            data = list(data.values())[0]
        except (AttributeError, IndexError, TypeError, ValueError):
            logger.warning("Can't parse Centaur json %s" % url)
            return

        self.packet_number = self._parse_int(data, 'controller/numberPackets')
        self.memory.size = self._parse_int(data, 'controller/ppc/memory/total')
        self.memory.free = self._parse_int(data, 'controller/ppc/memory/free')
        self.store.percentage_used = \
            self._parse_float(data, 'controller/store/storePercentageUsed')
        self.mass_1 = \
            self._parse_float(data, 'digitizer/sensor/soh/voltage#_1')
        self.mass_2 = \
            self._parse_float(data, 'digitizer/sensor/soh/voltage#_2')
        self.mass_3 = \
            self._parse_float(data, 'digitizer/sensor/soh/voltage#_3')
        self.external_soh_voltage_1 = \
            self._parse_float(data, 'externalSoh/voltage#_1')
        self.external_soh_voltage_2 = \
            self._parse_float(data, 'externalSoh/voltage#_2')
        self.external_soh_voltage_3 = \
            self._parse_float(data, 'externalSoh/voltage#_3')
        self.satellites = self._parse_int(data, 'gps/numberOfSatellites')
        self.internal_storage.free = \
            self._parse_int(data, 'media/freeSpace/internal')
        self.os_storage.free = self._parse_int(data, 'media/freeSpace/os')
        self.removable_SD_storage.free = \
            self._parse_int(data, 'media/freeSpace/removableSD')
        self.system_voltage = self._parse_float(data, 'powerSupply/voltage')
        self.system_current = self._parse_float(data, 'system/current')
        self.system_temperature = self._parse_float(data, 'temperature')
        self.dac_count = self._parse_int(data, 'timing/dacCount')
        self.time_error = self._parse_float(data, 'timing/timeError')
        self.time_quality = self._parse_float(data, 'timing/timeQuality')
        self.time_uncertainty = \
            self._parse_float(data, 'timing/timeUncertainty')

        self.memory.calculate_used()
        self.memory.calculate_percentage_free()
        self.memory.calculate_percentage_used()

# -*- coding: utf-8 -*-
"""
Taurus monitoring core support.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""
from datetime import timedelta
import logging
import math
import time

import aiohttp
import asyncio
import async_timeout
from lxml import etree

from .device import Device, DeviceData, Storage


logger = logging.getLogger(__name__)


class Taurus(Device):
    def init_name(self):
        self.name = 'taurus'

    def init_data(self):
        self.data = TaurusData()


class TaurusData(DeviceData):
    """
    Store Taurus status data
    """
    AMPERES = {
        'A': 1,
        'mA': 1e-3,
        'μA': 1e-6,
        'nA': 1e-9,
    }

    BYTES = {
        'B': 1,
        'KB': 1024,
        'MB': 1024**2,
        'GB': 1024**3,
        'TB': 1024**4,
    }

    SECONDS = {
        's': 1,
        'ms': 1e-3,
        'μs': 1e-6,
        'ns': 1e-9,
    }

    VOLTS = {
        'V': 1,
        'mV': 1e-3,
        'μV': 1e-6,
        'nV': 1e-9,
    }

    UNITS = {**AMPERES, **BYTES, **SECONDS, **VOLTS}

    def __init__(self):
        super().__init__()
        self.store_time_left = None
        self.store = Storage()
        self.system_power = None
        self.system_temperature = None
        self.system_voltage = None
        self.sensor_current = None
        self.digitizer_current = None
        self.system_current = None
        self.time_uncertainty = None
        self.time_error = None
        self.dac_count = None
        self.satellites = None
        self.pdop = None
        self.tdop = None
        self.mass_1 = None
        self.mass_2 = None
        self.mass_3 = None
        self.sensor_power = None

    def reset(self):
        super().reset()
        self.store_time_left = None
        self.store.reset()
        self.system_power = None
        self.system_temperature = None
        self.system_voltage = None
        self.sensor_current = None
        self.digitizer_current = None
        self.system_current = None
        self.time_uncertainty = None
        self.time_error = None
        self.dac_count = None
        self.satellites = None
        self.pdop = None
        self.tdop = None
        self.mass_1 = None
        self.mass_2 = None
        self.mass_3 = None
        self.sensor_power = None

    async def _get_root(self, session, url):
        """
        Retrieve the HTML page of the Taurus device.
        """
        try:
            with async_timeout.timeout(Device.TIMEOUT):
                async with session.get(url) as response:
                    html_data = await response.text()
        except asyncio.TimeoutError:
            logger.warning('TimeoutError for %s' % url)
            return None
        except aiohttp.ClientError:
            logger.warning("Can't connect to %s" % url)
            return None

        try:
            parser = etree.HTMLParser()
            return etree.fromstring(html_data, parser)
        except etree.XMLSyntaxError:
            logger.warning('Error while parsing XML %s' % url)
            return None

    def _parse_int(self, root, path, factor=1, divisor=1):
        value = None
        try:
            element = root.xpath(path)[0].split()
            value = int(element[0]) * factor // divisor
        except (IndexError, TypeError, ValueError):
            return None

        try:
            unit = element[1]
            value = value * TaurusData.UNITS[unit]
        except (IndexError, KeyError):
            pass

        return value

    def _parse_float(self, root, path, factor=1, divisor=1):
        value = None
        try:
            element = root.xpath(path)[0].split()
            value = float(element[0]) * factor / divisor
        except (IndexError, TypeError, ValueError):
            return None

        try:
            unit = element[1]
            value = value * TaurusData.UNITS[unit]
        except (IndexError, KeyError):
            pass

        return value

    async def _get_status_page_data(self, session, address, port):
        status_url = 'http://%s:%s/pages/taurus/status.page' % (address, port)
        root = await self._get_root(session, status_url)
        if root is None:
            return

        try:
            store_time_left_element = \
                root.xpath("//td/span[@id='lv_2']/text()")
            day_left, time_left = store_time_left_element[0].split('d')
            try:
                time_left = time.strptime(time_left.strip(), '%H:%M:%S')
            except ValueError:
                time_left = time.strptime(time_left.strip(), '%H:%M')
            time_seconds = timedelta(hours=time_left.tm_hour,
                                     minutes=time_left.tm_min,
                                     seconds=time_left.tm_sec).total_seconds()
            self.store_time_left = int(day_left) * 86400 + time_seconds
        except (IndexError, TypeError, ValueError):
            pass

        try:
            store_used_element = root.xpath("//td/span[@id='lv_3']/text()")[0]
            store_used, unit = store_used_element.split()
            self.store.used = int(float(store_used) * TaurusData.BYTES[unit])

            store_size_element = root.xpath("//td/span[@id='lv_4']/text()")[0]
            store_size, unit = store_size_element.split()
            self.store.size = int(float(store_size) * TaurusData.BYTES[unit])
        except (IndexError, TypeError, ValueError):
            pass

        self.store.calculate_free()
        self.store.calculate_percentage_free()
        self.store.calculate_percentage_used()

        self.system_power = \
            self._parse_float(root, "//td/span[@id='lv_8']/text()")

    async def _get_soh_page_data(self, session, address, port):
        soh_url = 'http://%s:%s/pages/taurus/soh.page' % (address, port)
        root = await self._get_root(session, soh_url)
        if root is None:
            return

        self.system_temperature = \
            self._parse_float(root, "//td/span[@id='lv_0']/text()")
        try:
            if math.isclose(self.system_temperature, -273):
                self.system_temperature = None
        except TypeError:
            # Already a None
            pass
        self.system_voltage = \
            self._parse_float(root, "//td/span[@id='lv_1']/text()")
        self.sensor_current = \
            self._parse_float(root, "//td/span[@id='lv_2']/text()")
        self.digitizer_current = \
            self._parse_float(root, "//td/span[@id='lv_3']/text()")
        self.system_current = \
            self._parse_float(root, "//td/span[@id='lv_4']/text()")

    async def _get_timing_page_data(self, session, address, port):
        timing_url = \
            'http://%s:%s/pages/taurus/timingIndex.page' % (address, port)
        root = await self._get_root(session, timing_url)
        if root is None:
            return

        self.time_uncertainty = \
            self._parse_float(root, "//td/span[@id='lv_2']/text()")
        self.time_error = \
            self._parse_float(root, "//td/span[@id='lv_3']/text()")
        self.dac_count = self._parse_int(root, "//td/span[@id='lv_4']/text()")
        self.satellites = \
            self._parse_int(root, "//td/span[@id='lv_6']/text()")
        self.pdop = self._parse_float(root, "//td/span[@id='lv_7']/text()")
        self.tdop = self._parse_float(root, "//td/span[@id='lv_8']/text()")

    async def _get_sensor_page_data(self, session, address, port):
        sensor_url = 'http://%s:%s/pages/taurus/sensor.page' % (address, port)
        root = await self._get_root(session, sensor_url)
        if root is None:
            return

        mass_1_path = "//td/b[text()='Mass 1:']/../following::td//td/text()"
        self.mass_1 = self._parse_float(root, mass_1_path)

        mass_2_path = "//td/b[text()='Mass 2:']/../following::td//td/text()"
        self.mass_2 = self._parse_float(root, mass_2_path)

        mass_3_path = "//td/b[text()='Mass 3:']/../following::td//td/text()"
        self.mass_3 = self._parse_float(root, mass_3_path)

        sensor_power_path = "//th[text()='Power:']/following::td//td/text()"
        self.sensor_power = self._parse_float(root, sensor_power_path)

    async def load_data(self, session, address, port=80):
        """
        Return a Taurus object containing the monitored data.
        """
        coroutines = [
            self._get_status_page_data(session, address, port),
            self._get_soh_page_data(session, address, port),
            self._get_timing_page_data(session, address, port),
            self._get_sensor_page_data(session, address, port),
        ]
        await asyncio.wait(coroutines)

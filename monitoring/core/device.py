# -*- coding: utf-8 -*-
"""
Device monitoring core support.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""
from abc import ABC, abstractmethod
import logging
import re

import asyncio
import async_timeout


logger = logging.getLogger(__name__)


class Device(ABC):
    """
    Represent a seismologic digitilizer (Q300, Taurus,…).
    """
    TIMEOUT = 30

    def __init__(self):
        self.init_name()
        self.init_data()

    @abstractmethod
    def init_name(self):
        pass

    @abstractmethod
    def init_data(self):
        pass

    async def load_data(self, session, address, port):
        self.data.reset()

        coroutines = [
            self.data.get_ping(address),
            self.data.load_data(session, address, port),
        ]
        await asyncio.wait(coroutines)


class DeviceData:
    """
    Store data of a device
    """
    def __init__(self):
        self.ping = None

    def reset(self):
        """
        Remove all values of a data object
        """
        self.ping = None

    async def get_ping(self, address):
        """
        Send a ping request
        """
        process = await asyncio.create_subprocess_exec(
            'ping', '-c', '1', address, stdout=asyncio.subprocess.PIPE)
        try:
            with async_timeout.timeout(Device.TIMEOUT):
                stdout, stderr = await process.communicate()
        except asyncio.TimeoutError:
            logger.warning('TimeoutError when pinging %s' % address)
            return

        raw_data = stdout.decode()

        try:
            self.ping = float(re.search(r'/([0-9\.]+)/', raw_data).group(1))
            # Convert from ms to s
            self.ping = self.ping / 1000
        except (AttributeError, IndexError, ValueError):
            logger.warning("Can't ping %s" % address)


class Storage:
    """
    Store storage informations (size, free space, used space,
    percentage of free space, percentage of used space).
    """
    def __init__(self):
        self.size = None
        self.free = None
        self.used = None
        self.percentage_free = None
        self.percentage_used = None

    def reset(self):
        self.size = None
        self.free = None
        self.used = None
        self.percentage_free = None
        self.percentage_used = None

    def calculate_size_from_percentage_free(self):
        """
        Calculate the storage size from the free space and percentage
        of free space.
        """
        try:
            self.size = int((self.free / self.percentage_free) * 100)
        except (TypeError, ZeroDivisionError):
            pass

    def calculate_free(self):
        """
        Calculate the free space from the storage size and the used
        space.
        """
        try:
            self.free = self.size - self.used
        except TypeError:
            pass

    def calculate_used(self):
        """
        Calculate the used space from the storage size and the free
        space.
        """
        try:
            self.used = self.size - self.free
        except TypeError:
            pass

    def calculate_percentage_free(self):
        """
        Calculate the percentage of free space from the free space and
        the storage size.
        """
        try:
            self.percentage_free = (self.free / self.size) * 100
        except (TypeError, ZeroDivisionError):
            pass

    def calculate_percentage_used(self):
        """
        Calculate the percentage of used space from the used space and
        the storage size.
        """
        try:
            self.percentage_used = (self.used / self.size) * 100
        except (TypeError, ZeroDivisionError):
            pass

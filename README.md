# monitoring

## InfluxDB

Retrieve monitoring data from Centaur, Güralp, Q330 or Taurus device and store
them in an InfluxDB database.
[Telegraf](https://github.com/influxdata/telegraf) is used to insert data in
the InfluxDB database. It allows to monitor other devices in a single docker.

### Install

Use `pip` or the `Dockerfile`:

```bash
pip install .
```

or

```bash
docker build -t monitoring .
```

### Usage

#### `influxdb_importer` script

A json configuration file is defined with the `CONFIG_FILE` environnement
variable. It is used to specify which devices needs to be monitored:

```json
{
    "XX.STA.01": {
        "device": "centaur",
        "host": "hostname",
        "projects": ["RESIF"],
        "supply": "mains"
    },
    "XX.ST2.": {
        "device": "taurus",
        "host": "hostname2",
        "supply": "photovoltaic panel",
        "port": 8080
    }
}
```

> **Info**
>
> The `supply` parameter describe the supply source (`mains` or
> `photovoltaic panel`).

The Telegraf configuration file must have the **exec** input plugin:

```ini
[[inputs.exec]]
  commands = ["influxdb_importer"]

  ## Timeout for each command to complete.
  timeout = "120s"

  # Data format to consume.
  # NOTE json only reads numerical measurements, strings and booleans are ignored.
  data_format = "influx"
```

With Docker, volumes can be used for both config files:

```bash
docker run -v /path/to/config.json:/opt/monitoring/config.json:ro -v /path/to/telegraf.conf:/etc/telegraf/telegraf.conf:ro monitoring
```

#### `check_device` script

This script checks that data can be retrieved from a device:

```bash
$ check_device q330 hostname
current {'device': 'system'}: 0.07
current {'device': 'gps_antenna'}: 0.007
initial_vco: 2245.96435546875
mass {'mass': 'mass_1'}: 1.1
mass {'mass': 'mass_2'}: 0.8
mass {'mass': 'mass_3'}: 1.1
mass {'mass': 'mass_4'}: 2.0
mass {'mass': 'mass_5'}: 2.0
mass {'mass': 'mass_6'}: 2.0
ping: 0.041866
satellites: 5
storage {'storage_type': 'dp_memory_0'}:
    free: 3342336
    size: 3342336
    percentage_used: 0.0
    used: 0
    percentage_free: 100.0
storage {'storage_type': 'dp_memory_1'}:
    free: 0
    size: 0
    used: 0
storage {'storage_type': 'dp_memory_2'}:
    free: 0
    size: 0
    used: 0
storage {'storage_type': 'dp_memory_3'}:
    free: 5046098
    size: 5046098
    percentage_used: 0.0
    used: 0
    percentage_free: 100.0
temperature {'device': 'system'}: 20.0
time_error: 0.0
time_quality: 90.0
uptime {'device': 'system'}: 120248444
voltage {'device': 'system'}: 13.2
```

## Grafana

Some dashboards can be found in `misc/dashboards`.

## crc_calc tool

The Q330 digitizer need a hash in the requests which can be generated with the
[crc_calc](misc/crc_calc) tool. See Q330 documentation for more details.
